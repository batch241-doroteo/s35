const express =  require('express');
const mongoose = require('mongoose');
const register = express();
const port = 3500;

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.8gw8qty.mongodb.net/s35_activity?retryWrites=true&w=majority", {
	// Due to update in MongoDB drivers that allow connection to it, the default connection is being flagged as error
	// Allows us to avoid any current and future errors while connectiong to MongoDB
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'))
//  if the connection is successful, print the message 'Connected to MongoDB Atlas!'
db.once('open', () => console.log('Connected to MongoDB Atlas!'))

// Allows your apps to read json data.
register.use(express.json())

// Allows your apps to read other data types.
// {extended:true} - by applying option, it allows us to receive information in other data types.
register.use(express.urlencoded({extended: true}))

const taskSchema = new mongoose.Schema({
	username: String,
	password: String
});

const Task = mongoose.model("Task", taskSchema);



register.post("/signup", (request, response) => {
	// To check if there are duplicate task
	Task.findOne({username: request.body.username}, (error, result) => {

		if(result != null && result.username == request.body.username) {
			return response.send('Username already exist!'); }
		else {
			let newTask = new Task({
				username: request.body.username,
				password: request.body.password
			});

			newTask.save((saveError, savedTask) => {
				if(saveError) {
					return console.log(saveError)
				} else {
					return response.status(201).send('New user registered')
				};
			}); 
		};
	});
});

register.listen(port, () => console.log(`Server running at port ${port}`))